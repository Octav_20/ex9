package ro.orangetraining;
import java.util.*;
import java.util.stream.*;
import java.lang.String;

public class Main {

    public static void main(String[] args) {

        Set<String> myEmployeesSet1 = new HashSet <>(Arrays.asList("Samuel Smith", "Bill Williams", "Anthony Johnson", "Cartner Caragher"));

        // printam
        System.out.println("myEmployeesSet1: "+myEmployeesSet1);

        //List
        List<String> list=new ArrayList<>(Arrays.asList("Astrid Conner",  "Christopher Adams","Antoine Griezmann", "Adam Sandler","Bailey Aidan","Carl Edwin"));

        //al doilea hash

        String[] l3=new String []{"Carl Edwin", "Adam Sandler", "Astrid Conner", "Bailey Aidan", "Christopher Adams","Antoine Griezmann"};
        //Set<String> myEmployees2 = new HashSet<>();
        Set<String> myEmployeesSet2= new HashSet<>(Arrays.asList(l3));
        //print
        System.out.println("list: "+list);
        System.out.println("myEmployeesSet2: "+myEmployeesSet2);
        //egal
        System.out.println("myEmployeesSet1 matches myEmployeesSet2: " +myEmployeesSet1.equals(myEmployeesSet2));
        myEmployeesSet2.remove(l3[1]);
        System.out.println("myEmployeesSet2: "+myEmployeesSet2);
        System.out.println("myEmployeesSet1 matches myEmployeesSet2: " +myEmployeesSet1.equals(myEmployeesSet2));
        //check with list
        System.out.println("myEmployeesSet1 contains all the elements: "+ myEmployeesSet1.containsAll(list));
        System.out.println("myEmployeesSet2 contains all the elements: "+ myEmployeesSet1.containsAll(list));
        //iterator
        Iterator<String> interator=myEmployeesSet1.iterator();
        while (interator.hasNext()){
            System.out.println("Iterator loop: "+ interator.next());
        }

        //clear
        myEmployeesSet1.clear();
        System.out.println("myEmployeesSet1 is Empty: " + myEmployeesSet1.isEmpty());
        //elelmente
        System.out.println("myEmployeesSet1 has: "+ myEmployeesSet1.size()+ " Elements");
        System.out.println("myEmployeesSet2 has: "+ myEmployeesSet2.size()+ " Elements");
        //
        String[] Array=new String[myEmployeesSet2.size()];
        myEmployeesSet2.toArray(Array);
        System.out.println(Arrays.toString(Array));


    }
}
